---
title: General Hardware
subtitle: 
tags: [cleaning]
---

  
General periodic maintenance is required, proportional to wear, on all hardware supplied such as locks, hinges, catches, rollers, door closers and the like. The external finish of all hardware must be kept clean by removing any harmful residue on the surface, especially salt spray, using a non-abrasive cleaning agent.
  
Internal workings of locks, catches etc. should be kept in good working order by applying a light spray of lubricant as specified by the manufacturer. Care should be taken to ensure that any finished surfaces (e.g. paint), in close proximity to the hardware being maintained, are well protected to avoid damage to 
the finish.