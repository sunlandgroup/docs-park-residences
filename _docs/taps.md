---
title: Taps, Spouts & Shower Roses
subtitle: Sanitary hardware should be cleaned regularly with household cleaners and plenty of water. 
tags: [cleaning]
---
### Caring for your Taps, Spouts & Shower Roses

**Caution**
Beware of scratching the surface of the hardware with abrasive products and cleaning utensils. If products inclusive of _(but not limited to)_ the following come into contact with your hardware, chemical damage/staining may occur:
  
- Iodine
  
- Mercurochrome
  
- Shoe Polish
  
- Hair Dye
  
- Bleach
  
- Nail Polish/Remover.