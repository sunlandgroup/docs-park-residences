---
title: Hot & Cold Water System
subtitle: 
tags: [services]
---
Your home has been installed with an instantaneous 	hot water system. Water is heated as it passes through 	the unit, providing you with a continuous flow of hot water.
Please note there are no exhaust fans in bathrooms that have a window.
  