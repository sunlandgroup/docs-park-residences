---
title: Stone Bench Tops
subtitle: Warranties will need to be registered. Please refer to the manufacturer’s pamphlet located in your settlement pack.
tags: [cleaning]
---
#### Caring for your Stone Bench Tops
  
Stone bench tops need little more than washing with cold water and a sponge, followed by wiping with a dry cloth. 

The surface should never be allowed to get dirty enough to require more thorough cleaning. Weekly cleaning is recommended to maintain the stone’s appearance and should be repeated more regularly if required by the level  of soiling. All spillages should be cleaned immediately  to prevent any liquid/chemicals penetrating the stone’s pores, and resulting in unsightly staining. Bench tops should not be used as food preparation areas/cutting boards, as the stone (whether natural or reconstituted) will eventually mark and stain. Food preparation should be carried out on cutting boards, which may be replaced when marked and stained.
  
With regards to bathrooms, ensuites and laundry applications, bleaches and mould-removing cleaning products should not be applied to any marble, limestone, granite and/or reconstituted stone. Care should be taken with toilet blues.
  
Stains that cannot be removed from stonework by normal cleaning procedures should be addressed by 
a specialist.
  
**To care for your stonework you should:**
  
- Polish or reseal the stonework periodically as part of your routine maintenance.
  
- Ensure dirt and grease is not left to accumulate as damage and cracks to the surface may appear.
  
- Avoid harsh or abrasive cleaners.
  
- Remove spills immediately.
  
- Avoid acid-based products that will react with the calcium carbonate in the stonework and will damage the polished surface.
  
- Avoid the use of detergents with a high pH rating 
as they will attack the resin of the stonework.
  
- Use water to wash away all traces of cleaning products.
  
- Remove common dirt and soil daily – care must be taken to ensure any grit is quickly removed from the stonework as it can damage the surface.
  
**Caution**
  
Do not sit or stand on stone bench tops where there 
is a recess i.e. kitchen sink, hand basin or cook top.
  
The manufacturer’s warranty will not be honoured for breaks in these areas. Do not put hot pans on bench tops directly from your cook top.