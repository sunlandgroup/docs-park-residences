---
title: Kitchen Sink
subtitle: Scraping and banging metal kitchen utensils in the sink will gradually scratch and dull its surface, making 	the sink more susceptible to staining.
tags: [cleaning]
---
#### Caring for your Kitchen Sink
  
 Scouring material and harsh abrasives should NOT be used for cleaning stainless steel. The surface responds best to the gentlest cleaning methods. Frequent washing with water (preferably hot) is the recommended treatment, followed by rinsing with fresh water and wiping dry with a soft cloth or paper towel.
  
Avoid leaving ordinary metal items (e.g. metal cans) in the kitchen sink for prolonged periods of time, as these may mark the surface with rust from the item. Rust marks can be very difficult to remove without damaging the polished surface of the sink.