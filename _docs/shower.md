---
title: Shower Areas
subtitle: Due to the constant use of shower areas, regular cleaning is advised to avoid heavy-duty cleaning, and for hygiene purposes. 
tags: [cleaning]
---
### Cleaning your Shower

Routine build-up can be removed with most all-purpose cleaners, while hard-water deposits are best removed with a solution of white vinegar and water. Bacteria and mould can develop due to the damp nature of shower recesses. This can be removed by wiping down the areas with a chlorine bleach product. Apply according to the manufacturer’s instructions and rinse with clear water.
  
**Caution**
  
When using bleach products caution should be taken and it should never be mixed with ammonia. Always use chlorine in a well-ventilated room.
  