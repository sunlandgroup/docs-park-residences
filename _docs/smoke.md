---
title: Smoke Detectors
subtitle: Keep Your property safe and learn how to operate and test your smoke alarm.
tags: [emergencies]
---

Smoke detectors have been installed in your home. Generally, they are located on the ceiling in the corridor near the kitchen or in the living area directly outside the bedrooms. They are connected to a 240V power supply via your switchboard, with a battery backup. Your smoke detectors will sound a localised alarm if smoke is detected in your property. *They will not notify the Fire Brigade.*
  
### Smoke detectors require weekly testing. 
#### To test the alarm 

```bash
Push and hold the TEST button for several seconds
```

#### To silence the alarm 

```bash
Press the HUSH button
```

The smoke alarm uses a 9V battery to automatically provide backup power to the alarm in the event of a power failure. Smoke alarms will beep intermittently when the battery is low, however batteries should be replaced once every year. April 1st is the recommended changeover date for batteries. For more detailed information, refer to the user instructions found in your warranty pack.

  
