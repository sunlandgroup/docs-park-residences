---
title: Window Frames & Doors
subtitle: Door and window tracks should be cleaned regularly to avoid permanent damage to opening/closing mechanisms. 
tags: [cleaning]
---
### WINDOW FRAMES/DOORS/ DOOR FURNITURE/HARDWARE

 Glass and window frames will always look their best if they are regularly cleaned with suitable products. Clear, anodised or powder-coated surfaces should be cleaned with mild detergent. 

**Caution**
Avoid the use of detergents that contain acidic products as these discolour the anodised/powder coated finish of the window and door frames. Never use paint removers, aggressive alkaline, acid or abrasive cleaners. Do not use trisodium phosphate or highly alkaline or highly acidic cleaners. Always test cleaners in an area out of sight first. Follow the manufacturer’s directions for mixing and diluting cleaners. Never mix cleaners – doing so may not only be ineffective, but also very dangerous, as some chemicals have violent reactions when mixed. 
  
Wipe down handles occasionally, using stainless polish for stainless-steel handles. Stiff mechanisms may need to be lubricated with dry lubricant  (consult your locksmith). Loose mechanisms may need to have screws tightened, especially during periods of heavy use.