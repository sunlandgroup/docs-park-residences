---
title: Flooring
subtitle: 
tags: [cleaning]
---
### Carpet

##### Regular maintenance is required for carpets.
  
**Carpet cleaning should occur as follows:**
  
- Carpet should be vacuumed weekly, or as required, to prevent dirt particles from getting into the carpet fibres.
  
- Spot cleaning should be undertaken immediately after soiling by spillage or dirt; use a warm, damp, clean cloth.
  
- When using carpet-stain removers, you should ensure the manufacturer’s instructions are followed at all times.
  
Periodic deep cleaning of carpets by carpet-cleaning experts is also recommended (at least annually).

### Floor/Wall Tiles
  
Please take care when moving about on stone and ceramic tiled floors, as they can be extremely slippery when wet. The ease of maintenance of any tiled surface is dependent on both the colour and texture of the surface. Regular maintenance of all tiled surfaces should be undertaken 	to avoid the need for aggressive chemical products.
  
- DO NOT clean tiles with acid
  
- DO NOT clean tiles with any abrasive materials
  
- DO NOT place potted plants directly on to tiled areas and remove any residue immediately before staining occurs. 
  
Use specifically designed cleaning detergents only.