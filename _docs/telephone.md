---
title: Telephone, TV And Internet Connection
subtitle: Your residence has been pre-wired to allow easy connection to telephone networks. 
tags: [general]
---
### Getting Connected
  
Contact one of the following service providers (or your preferred supplier) to arrange connection of your telephone line, internet and provision of handset(s) if required:
  
**Telstra**   
[13 22 00](tel:132200)
  
**Optus**   
[13 39 37](tel:133937)
  
**Free to Air Television**
  
Free to air TV services are already activated, so all you need to do is connect your digital TV to the Broadcast Outlet and tune in the channels.
  
**Pay TV**
  
Your home has the capacity to receive a Pay-TV service. The Pay-TV company providing this service is Foxtel. Residents wishing to access this service can contact the provider for details of available services/payment options and installation of decoder box.