---
title: 24-hr Medical Emergency Care
subtitle: For assistance during a disaster, there are several emergency services contact numbers you may need.
tags: [contacts]
---
 
**Gold Coast University Hospital**  
Southport    
[1300 744 284](tel:1300744284)
(24-hour emergency room)
  
**Gold Coast Private Hospital**  
Southport   
[07 5530 0300](tel:07 5530 0300)
  
**Pindara Private Hospital**   
Benowa  
[07 5588 9888](tel:075588 9888)
  
**National Home Doctor Service**   
[137 425](tel:137425) (non-urgent after hours bulk-billing)