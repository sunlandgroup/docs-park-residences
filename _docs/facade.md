---
title: Façade Products Powder Coating
subtitle: Powder coating needs to be given a regular wash to remove dirt and grime and to keep it looking in top condition. As a general rule, cleaning should take place each six months. In areas where pollutants are prevalent, cleaning should be carried out more frequently, paying particular attention to areas that are not normally washed by rain.
tags: [cleaning]
---
#### Three steps to cleaning powder-coated surfaces:
  
1. Remove any loose deposits with a wet sponge rather than risk micro-scratching the surface by dry dusting.
  
2. Using a soft brush and mild detergent in warm water, clean the powder-coating to remove any dust, salt or other deposits.
  
3. Rinse thoroughly with clean, fresh water to remove any remaining detergent, etc.
  
#### Caution
  
The use of harsh solvents may damage the integrity of the powder coating. It is recommended that you use methylated spirits or mineral turpentine on stubborn stains.