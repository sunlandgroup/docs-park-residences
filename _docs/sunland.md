---
title: Sunland Group
subtitle: Key contacts within Sunland Group.
tags: [contacts]
---
 

**Sunland Group**  
PO Box 1301,    
Surfers Paradise,    
Queensland 4217   
reception@sunlandgroup.com.au   
[1300 744 284](tel:1300744284)  

**Client Relations**   
[07 5564 3700](tel:0755643700])
  
**Maintenance Co-ordinator**    
[07 5564 3700] (tel:0755643700)   
qldmaint@sunlandgroup.com.au