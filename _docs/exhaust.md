---
title: Exhaust System
subtitle: 
tags: [services]
---
Your property is fitted with an exhaust fan in the bathrooms and ensuite which are operated when the switch is turned on in the respective areas. However, it is recommended that you leave the laundry door open while using your clothes dryer, as moisture may cause damage to the laundry doors and internals in the long term. Kitchen range hoods operate normally, assisted via either ducting to the atmosphere or recirculating _(where applicable)_.
  
To ensure correct operation of the exhaust system:
  
- Check air inlets for obstructions at all times.
  
- Check for correct operation and noise monthly.
  
Please note there are no exhaust fans in bathrooms that have a window.
  