---
title: Schools and Universities
subtitle: For assistance during an emergency or disaster or electricity and water—reporting outages and faults.
tags: [amenities]
---
 

### Primary & Secondary Education
  
**Pimpama State Primary School**   
Pimpama   
[07 5549 4333](tel:0755494333)
  
**Pimpama State Primary College**   
Pimpama   
[07 5549 5333](tel:0755495333)
  
**Pimpama State Secondary College**   
Pimpama   
[07 5540 9333](tel:0755409333)
  
**Kings Christian College**   
Pimpama   
[07 5587 7660](tel:0755877660)
  
**Coomera Anglican College**   
Upper Coomera   
[07 5585 9900](tel:0755859900)
  
**Saint Stephen’s College**   
Upper Coomera  
 [07 5573 8600](tel:0755738600)
  
**Assisi Catholic College**   
Upper Coomera   
[07 5656 7100](tel:0756567100)
  
**Upper Coomera State College**   
Upper Coomera    
[07 5580 7555](tel:0755807555)
  
**Ormeau State School**   
Ormeau   
 [07 5546 6500](tel:0755466500)
  
**Livingstone Christian College**   
Ormeau    
[07 5546 7083](tel:0755467083)
  
**Mother Teresa Primary School**   
Ormeau    
[07 5549 5000](tel:0755495000)
  
### Tertiary Education
  
**Griffith University**   
Southport    
[07 5552 8800](tel:0755528800)
  
**Bond University**   
Robina    
[07 5595 1111](tel:0755951111)
  
**TAFE Creative Campus**   
Coomera    
[07 5581 8300](tel:0755818300)
  
**Gold Coast Institute of TAFE**    
Ashmore    
[07 5581 8200](tel:0755818200)