---
title: Maintenance and Building Warranty
subtitle:
tags: [maintenance]
---

A pre-settlement inspection is offered to owners 1-2 weeks prior to settlement. Other maintenance items not completed or identified at this time can form part of the Post Settlement Building Warranty Reporting Form.

### Warranty Periods
  
Building warranty work usually falls into two categories:
   
- Structural warranty - valid for six years and six months from practical completion.
  
- Non-structural warranty - valid for 12 months from practical completion.
  
Practical completion is deemed as the date of the final building inspection by Council or Certifier.
Please refer to [qbcc.qld.gov.au](www.qbcc.qld.gov.au) for more details.
  
Defects do not include damage by any owner, resident, or any other party.
  
In the case where Sunland Group has sold the property 10 months post the practical completion date, the purchaser will be granted a non-structural warranty valid for three (3) months from the date of their settlement.
  
### Making a Warranty Claim
  
In order to claim under these warranties, the owner must notify Sunland of such defects in writing via the Post Settlement Building Warranty Reporting Form provided to you in your settlement pack after settlement, or via email to the Maintenance Coordinator (refer below).
  
Tenants must refer any defects to their Property Manager (or Owner) who will contact Sunland.
  
Items arising out of fair and reasonable wear and tear remain the responsibility of the owner/occupier and are not covered by this warranty.
  
The owner or occupier must give Sunland and its agents access to the property during reasonable times (Monday to Friday between 7am and 3.30pm) to enable inspection of the defects and carry out the rectification works. A dedicated Maintenance Coordinator can be contacted through Sunland’s office.