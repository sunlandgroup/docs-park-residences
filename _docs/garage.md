---
title: Garage Doors
subtitle: 
tags: [cleaning]
---
All garage doors should be serviced annually to maintain opening mechanism at its optimum level.
(Refer to section 5.1 for details on warranty).