---
title: Emergency Proceedures After Hours
subtitle: At all times emergency services can be contacted by telephoning 000 for assistance. Be prepared to identify yourself, your location, the problem and the likely emergency service required.
tags: [emergencies]
---

Firstly, identify the cause of the problem. If the problem is within your own home and if safe to do so, you can attempt to isolate the problem:

- If it is a water problem, shut off the water valve.
  
- If it is an electrical problem, please turn off the main power switch on the switchboard. 
  
- If it is a gas cooktop problem, please turn off the gas shut off valve.
  
If you experience a non-emergency maintenance/ service problem within your home, please refer to the supplier contact list in Section 6.3.
  
Please note that if you utilise an independent service/ repair contractor other than those provided within the Supplier List, warranties may be voided and you may incur service fees.
  
**CALL OUTS FOR NON-EMERGENCY ITEMS WILL BE CHARGED TO YOU DIRECTLY**
  
**Gold Coast City Council – Water**    
[1300 000 928](tel:1300000928)

**Electricity – Origin**   
[1300 661 544](tel:1300661544)

**Gas – Origin**   
[13 24 63](tel:132463)
  
Or contact your supplier

  
