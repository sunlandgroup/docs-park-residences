---
title: Painted Surfaces
subtitle: Quality paints have been used throughout your home. Proper care and cleaning must be carried out to ensure that the appearance and integrity of your paintwork is maintained. Avoid using excessive water and never use an abrasive cleaner or scourer. To remove dust, simply use a wall duster or feather duster and brush lightly.
tags: [cleaning]
---
To remove minor marks, wipe very lightly in a circular motion with a clean, damp, soft microfibre cloth. Soiled surfaces or light stains are best removed with a solution of sugar soap in warm water – sugar soap can be found at most hardware stores. Brushes of any kind should NOT be used to remove stains on painted surfaces. Avoid using Blu Tac or adhesive tape on painted surfaces as they may leave stains or cause the paint to flake upon removal.