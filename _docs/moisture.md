---
title: Moisture
subtitle: Many materials used in the construction of your dwelling contain moisture. 
tags: [cleaning]
---
### If you find a leak
As your dwelling is heated, lived in, dried out and settled in, small cracks may appear in timber, plaster and concrete elements as they shrink. Shrinkage/slight movement is a common occurrence in new dwellings and has no effect on your home’s structural integrity.
  
#### Condensation
  
Condensation occurs when the air temperature inside the dwelling is greater than the air temperature outside. 
  
**To prevent condensation:**
  
- Open windows in dry weather.
  
- Use the kitchen’s rangehood exhaust fans 
where possible.
  
- Ensure ceiling vents are not obstructed.
  
- Leave bathroom and laundry doors open 
where possible.
  
- Ensure all ceiling vents are not obstructed.