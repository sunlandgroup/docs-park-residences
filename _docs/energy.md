---
title: Energy Sources
subtitle:
tags: [appliance]
---

### The energy sources of your appliances are as follows
  
- Rangehood – Electricity
  
- Cooktop – Gas 
(with electricity provided for ignition)
  
- Oven – Electricity
  
- Dishwasher – Electricity
  
- Hot Water – Gas