---
title: Roof Leaks
subtitle:
tags: [maintenance]
---

Should a roof leak occur 12 months after settlement, Sunland Homes deem it to be the owner or tenant’s responsibility to rule out maintenance issues, such as a broken tile, installation of a satellite dish, or leaves in the gutters or valleys. In the event of Sunland Homes attending a leak which is deemed maintenance, then a call out fee may be charged.