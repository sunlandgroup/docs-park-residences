---
title: Bath and Basins
subtitle: Your bath and basins should be cleaned regularly with non-abrasive household cleaners and plenty of water.
tags: [cleaning]
---
### Caring for your bath and basins

  
Beware of scratching the surface of the bath/basin with abrasive products and cleaning utensils. Do not step into the bath or shower with shoes on,as grit may scratch the polished surface.

**Caution**  
If products inclusive of, but not limited to, the following come into contact with your bath/basin, chemical damage/staining may also occur:
  
- Iodine
  
- Mercurochrome
  
- Shoe Polish
  
- Hair Dye
  
- Bleach
  
- Nail Polish/Remover.
  
Spills from products such as aftershave lotion, hair lacquer, mineral turpentine, white spirit, kerosene, aerosol propellants and insecticides should be 	removed immediately to avoid discoloration.