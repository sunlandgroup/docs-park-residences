---
title: Shower Screens
subtitle: 
tags: [cleaning]
---
### Hinges and other hardware

The best way to maintain hinges and other hardware is to wipe them down after every use. If the hardware is kept clean, it will not collect mineral deposits that require the use of soap solution to remove. After showering, use a dry towel to thoroughly dry the hinges. If you are unable to dry the hinges after every use, a weekly clean is suggested. Use a mild soap and warm water mixture and a soft, non-abrasive cloth. After you have cleaned the hardware, rinse it thoroughly with clean, warm water and dry.
  
**Warning**
  
Never use abrasive cleanser of any kind on hinges and other hardware. Many of the components are coated with a clear lacquer that will be irreparably damaged if subjected to harsh abrasive chemicals or scrubbing devices.
  
### Glass
  
It is important to keep your glass clean. Glass that is neglected will accumulate water spots, which will eventually turn into mineral deposits. The best way to keep your glass free of potentially damaging water spots is to squeegee the glass after every use.
  
Regular cleaning, rather than allowing deposits to accumulate, will save you a great deal of work. 
Do not use any abrasive or cream cleaner.  