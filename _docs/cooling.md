---
title: Cooling/Heating
subtitle: 
tags: [services]
---
Split system air conditioning units have been installed to the living area and master bedroom in your home. Filters have to be cleaned regularly and the units maintained as per the owner’s operating manual. Annual servicing is recommended. For more information, please refer to the manuals provided.
  