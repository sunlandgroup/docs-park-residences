---
title: Appliance Manuals and Warranties
subtitle:
tags: [appliance]
---

### Appliances
Information relating to the high quality appliances and systems within your new home are outlined in the manuals contained within your settlement pack. 

All home appliances, including your oven, cooktop, rangehood, dishwasher and air conditioning are covered by the manufacturer’s warranty. If you discover that an appliance is not operating correctly or is faulty, please contact the manufacturer (for air conditioning, see note below). The manufacturer’s information, warranties and instruction booklets are provided in your settlement pack. 

It is the property owner’s responsibility to maintain these items and arrange repairs and maintenance if required. Repairs should be carried out by an authorised service company in accordance with the manufacturer’s specifications. 
  
Some manufacturers may require proof of purchase. Sunland’s maintenance coordinator can provide this if required. However, please note that an appliance defect is not a building warranty issue and that Sunland has no control over manufacturers servicing times and schedules.
  
_Note: There is a 12 month electrical contractors installation warranty on Air Conditioning units and for the first year any air conditioning issues should be reported to Sunland Homes first before making a manufacturer’s warranty call._
  
It is recommended that all manuals and warranties are kept in a safe place within the home.
  
### Garage Doors
  
Your garage door has a 12 month warranty from installation. It is recommended that garage doors be serviced annually. This will keep the garage door in optimum condition and maintain motor warranty.
  
Steel Line Garage Doors [07 5571 5566](tel:0755715566) will service your garage door at no cost within the first six months from practical completion.