---
title: Authority/Utilities Contacts
subtitle: For assistance during an emergency or disaster or electricity and water—reporting outages and faults.
tags: [contacts]
---
 

**SES (State Emergency Service)**  
Gold Coast   
[07 5591 1177](tel:07559111770)
  
**Policelink Reporting of Non-Urgent Incidents**   
[13 14 44](tel:131444)
  
**Coomera Police Station**    
11 De Barnett Street    
[07 5519 5555](tel:0755195555)
  
**Electricity – Origin**   
[1300 661 544](tel:1300661544)
  
**Gas – Origin**   
[13 24 63](tel:132463)
  
**Gold Coast City Council – Water**    
[1300 000 928](tel:1300000928)
  
**Gold Coast City Council – General (24/7)**    
[1300 465 326](tel:1300465326)