---
title: Healthcare
subtitle: Phone numbers to health professionals and hospitals .
tags: [amenities]
---
 


### Medical Centres
  
**Pimpama Medical Centre**   
Dixon Drive, Pimpama    
[07 5549 3100](tel:0755493100)
  
**Medicross Coomera**   
Days Rd, Upper Coomera  
 [07 5573 0911](tel:0755730911)
  
**Hope Island Medical Centre**   
Hope Island  
 [07 5510 8199](tel:0755108199)
  
  
### Hospitals
  
**Gold Coast University Hospital**   
Southport  
 [1300 744 284](tel:1300744284)
  
**Gold Coast Private Hospital**   
Southport  
 [07 5530 0300](tel:0755300300)
  
**Pindara Private Hospital**   
Benowa   
[07 5588 9888](tel:0755889888)

