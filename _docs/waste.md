---
title: Waste Bins
subtitle: You will need to arrange for your bin to be delivered by phoning the Gold Coast City Council.  
tags: [general]
---
  
**Gold Coast City Council**
Waste Management
[1300 694 222](tel:1300694222)
  
Emergency
[1800 637 000](tel:1800637000)