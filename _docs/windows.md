---
title: Windows
subtitle: Powder coating needs to be given a regular wash to remove dirt and grime and to keep it looking in top condition. As a general rule, cleaning should take place each six months. In areas where pollutants are prevalent, cleaning should be carried out more frequently, paying particular attention to areas that are not normally washed by rain.
tags: [cleaning]
---
### Cleaning your Windows
  
Apply a non-abrasive mild detergent solution to glass either by spraying or using a clean, grit-free cloth or sponge saturated with cleaning solution. Complete coverage of the area to be cleaned is necessary. 
  
Wipe the cleaning solution over the glass in a circular motion, applying light to moderate pressure. Approximately three to five passes of the affected area may be required, depending on the type and severity 	of the residue or sediment on the glass.
  
After cleaning with the solution, rinse the glass surface thoroughly with generous amounts of clean water, removing all traces of the cleaning solution from the glass surface. Using a squeegee or clean, lint-free cloth, remove water from the glass surface. If residue is still evident on the glass surface, repeat as above.
  
#### Important Notes
  
Do not clean the glass when it is exposed to direct sunlight. Glass should be cleaned by starting at the top of the pane and systematically working down to the bottom. This technique reduces the possibility of soiling previously cleaned glass.
  
Additional care should be exercised when cleaning all glass surfaces to ensure that gritty cloths, the metal parts of squeegees or other sharp, hard objects do not scratch the glass surface. Metal scrapers MUST NOT be used.

**Caution** 
What not to do:
  
1. Do not store or place items in contact with the glass, as this can damage it or cause a heat trap leading to thermal breakage.
  
2. Never use abrasive cleaners on glass; scouring pads or other harsh materials must not be used to clean windows or other glass products; powder-based cleaners are to be avoided.
  
3. Avoid extreme temperature changes, as this may lead to thermal fracture of the glass – do not splash hot water on cold glass, or freezing water on hot glass.
  
4. Some tapes or adhesives can stain or damage glass surfaces; avoid using such materials unless they are known to be easily removed.