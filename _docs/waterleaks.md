---
title: Water Leaks
subtitle: All water leaks have the potential to cause considerable damage if not repaired promptly. 
tags: [cleaning]
---
### If you find a leak
  
If you find a leak, isolate it where possible and arrange for repair or, if within warranty period, inform Sunland’s Maintenance Co-ordinator immediately on on [07 5564 3700](tel:0755643700), followed by confirmation in writing via 
email [qldmaint@sunlandgroup.com.au](mailto:qldmaint@sunlandgroup.com.au).
  
All sealants to showers, wet areas and tile junctions should be inspected regularly and maintained (where applicable).