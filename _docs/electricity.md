---
title: Electricity Connection
subtitle: 
tags: [services]
---
Origin is currently the local electricity supply authority. You are required to contact Origin to provide account details for records and billing of electricity to your dwelling and to arrange for power to be turned on. 
You can choose to connect with Origin or your preferred supplier.
  
You should notify Origin, or your preferred supplier, of your details within 14 days of settlement, to ensure that power supply is not terminated. After settlement date, any power that is used will be charged to your account accordingly.

### Getting your electricity connected 

**Electricity – Origin**
[1300 661 544](tel:1300661544)
(select option 1 moving in or out of a property)
  
24 Hour emergency
[13 19 62](tel:131962)
  
To turn power on in your home, make sure that all circuit breakers and the main switch are in the ON 
or UP position.
  
Should a trip-out occur, the appliance must be isolated from the power outlet and the circuit breaker turned to the ON position. Push the reset button on the safety switch. If the circuit breaker still trips out, an electrician should be called.
