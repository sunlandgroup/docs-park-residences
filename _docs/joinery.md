---
title: Joinery
subtitle: Joinery surfaces will dent or scratch if treated roughly.
tags: [cleaning]
---
#### Caring for your Joinery
  
Particular care should be taken when moving furniture or heavy objects near joinery. To clean the shelves and interiors of the cabinets, a liquid cleaner is recommended as it does not leave streak marks on 
the surfaces. 
  
Whenever stubborn spots occur that cannot be removed with a liquid cleaner, then the careful use  of a cream cleaner is recommended. Avoid placing heavy objects in the centre of the shelves, especially in the wider units, as this will make the shelves bow. Heavy objects should be placed to the sides of the shelves.