---
title: Sealants
subtitle: 
tags: [cleaning]
---

  
All sealants should be cleaned (wiped down with a damp cloth only) regularly and inspected for deterioration and required maintenance.