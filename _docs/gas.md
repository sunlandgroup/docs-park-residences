---
title: Gas Connection
subtitle: 
tags: [services]
---
It is the responsibility of the owner or occupier to apply for gas connection. The gas provider will then supply the owner or occupier with natural gas.
  
If you wish to stop the supply of gas to your property for any reason, there is a handled stop valve located on the top of the supply side of the meter – it is recommended you turn the valve off.
  
For ease of identification: if the handle points in the line with the gas pipe, then the supply valve is open. If the handle points at right angles to the gas pipe, then the supply valve is closed.

### Getting your gas connected 
**Gas – Origin**
[13 24 63](tel:132463)