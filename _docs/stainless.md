---
title: Stainless-Steel Surfaces
subtitle: Never use abrasive cleaners on stainless steel. Scouring pads, steel wool or other harsh materials must not be used to clean stainless steel. Powder-based cleaners are to be avoided.
tags: [cleaning]
---
### Cleaning your Stainless-Steel Surfaces
  
To clean stainless-steel surfaces, wipe gently with a damp, lint-free cloth or other commercially available stainless-steel cleaning products.