---
title: Checklist
subtitle: 
tags: [Moving]
---
 
### Immediate Organisation
  
- Home and Contents Insurance
  
- Electricity, Gas and Water connection
  
- Telephone, Internet and Television connection
  
- Update address for regular deliveries eg. paper
or magazine subscriptions
  
- Redirection of mail by Australia Post
  
- Keys
  
- Waste Bin Delivery
  
### Home and Family
  
- New school enrolments
  
- Transfer current school records
  
- Establish local doctor/dentist
  
- Transfer existing medical/dental records
  
- Update medical benefits office re: new address
  
- Transfer family youth activities 
(e.g. scouts, tennis etc)
  
### Personal
  
- Drivers License
  
- Insurance Company/Broker
  
- Bank Accounts
  
- Credit Unions
  
- Credit Card offices
  
- Retail Accounts 
(i.e. department store cards)
  
- Electoral Roll
  
- Roadside Assistance Membership