---
title: Emergency Contacts
subtitle: At all times emergency services can be contacted by telephoning 000 for assistance. Be prepared to identify yourself, your location, the problem and the likely emergency service required.
tags: [emergencies]
---


**Police**   
[000](tel:000)

  
**Ambulance**   
[000](tel:000)
  
**Fire Brigade**   
[000](tel:000)