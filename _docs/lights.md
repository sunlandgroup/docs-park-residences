---
title: Light Fittings
subtitle: Your light fittings are LED lights and therefore have no replacement globes. 
tags: [cleaning]
---
#### Replacing fittings
  
Fittings are easily replaced by the homeowner by removing the old fitting and plugging in a new fitting. 

#### Replacement of lights
Fittings shall always be the responsibility of the owner or tenant. Faulty fittings are covered by the manufacturer's warranty.