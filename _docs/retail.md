---
title: Shopping Precincts
subtitle: Location information for local retail and shopping outlets.
tags: [amenities]
---
 
  
**Coomera Square**   
Cnr Days Rd & Old Coach Rd, Coomera
  
**Upper Coomera City Centre**   
Days Rd, Upper Coomera
  
**Pimpama Junction**   
Cnr Yawalpah Rd and Dixon Dr, Pimpama
  
**Westfield Coomera Shopping Centre**  
Foxwell Road, Coomera
  
**Westfield Shopping Centre**  
Millaroo Drive, Helensvale
  
**Homeworld Helensvale**  
Hope Island Rd, Helensvale
  
**Harbour Town Shopping Centre**  
Cnr Gold Coast Hwy & Oxley Drive,
Biggera Waters
  
**Australia Fair**  
Marine Parade, Southport