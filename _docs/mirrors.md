---
title: Mirrors
subtitle: Mirrors require special care in cleaning.
tags: [cleaning]
---
### Cleaning your Mirrors

Do so by wiping over the surface with a few drops of methylated spirits on a damp cloth. Polish the surface dry with a lint-free cloth. Some proprietary glass cleaners, if used to excess, can cause damage to the silvering, as can excessive amounts of water. Make certain when cleaning the face of the mirror that there is no contact with the silver backing, particularly at the edge of the glass, and be careful to keep any moisture away from the paint backing of the mirror. Do everything possible to ensure that the cleaning cloths used are free of abrasives.
  