---
layout: page
width: expand
hero:
    title: How can we help you?
    subtitle: Search or browse information to help you settle in and familiarise yourself with your new home
    image: house_searching.svg
    search: true
---

{% include boxes.html columns="3" title="Browse Topics" subtitle="Chose an option that you need help with or search above" %}

<!-- {% include featured.html tag="featured" title="Popular Articles" subtitle="Selected featured articles to get you started fast in Jekyll" %} -->

<!-- {% include videos.html columns="2" title="Video Tutorials" subtitle="Watch screencasts to get you started fast with Jekyll" %} -->

{% include faqs.html multiple="true" title="Frequently asked questions" category="presale" subtitle="Find quick answers to frequently asked questions" %}

<!-- {% include team.html authors="evan, john, sara, alex, tom, daniel" title="We are here to help" subtitle="Our team is just an email away ready to answer your questions" %} -->

{% include cta.html title="Need a hardcopy?" button_text="Download" button_url="http://cdn.sunlandgroup.com.au/gold-coast/park-residences/Park-Residences-Home-Owners-Manual.pdf" subtitle="Download a PDF if you need to print out the Owners Manual" %}

