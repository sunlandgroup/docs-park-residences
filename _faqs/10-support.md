---
title: How do I use my Appliances?
categories: [presale]
---

#### Finding your appliance manuals
Information relating to the high quality appliances and systems within your new home are outlined in the manuals contained within your settlement pack.