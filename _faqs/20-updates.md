---
title: How long are Waranties?
categories: [presale]
---

### Warranty Periods

#### Maintenance and Building Warranty
**Building warranty work usually falls into two categories**

1. Structural warranty - valid for six years and six months from practical completion.

2. Non-structural warranty - valid for 12 months from practical completion.


#### Appliance Warranties

All home appliances, including your oven, cooktop, rangehood, dishwasher and air conditioning are covered by the manufacturer’s warranty. If you discover that an appliance is not operating correctly or is faulty, please contact the manufacturer (for air conditioning, see note below). The manufacturer’s information, warranties and instruction booklets are provided in your settlement pack.