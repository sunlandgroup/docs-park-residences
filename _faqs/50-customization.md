---
title: Who do I contact for standard fixtures and fittings?
categories: [presale]
---

**Listed below are suppliers of the standard fixtures and fittings within Park Residences (excluding purchaser variations).**
    
**Air-conditioning**   
General Air Conditioning  
[07 5580 1490](tel:0755801490])  
[info@generalair.com.au](mailto:info@generalair.com.au)
  
**Appliances**  
Harvey Norman  
[07 3297 3700](tel:0732973700)  
[julie.pittman@au.harveynorman.com](mailto:julie.pittman@au.harveynorman.com)  
  
**Mirrors, Showerscreens, Splashbacks**  
Civic Showerscreens and Wardrobes  
[07 3441 7711](tel:0734417711) 
[straddiehilton@bigpond.com](mailto:straddiehilton@bigpond.com) 
  
**Carpet**  
Carpet Call  
[07 3489 1333](tel:0734891333)  
[andrew.rodrick@carpetcall.com.au](mailto:andrew.rodrick@carpetcall.com.au)  
  
**Electrical**  
AMQ Electrical  
[0407 793 734](tel:0407793734)   
[micheal@amqelectrical.com](mailto:micheal@amqelectrical.com)
  
**Gas Hot Water System**  
Logic Plumbing and Drainage  
[07 5573 3996](tel:0755733996)  
[logic123@bigpond.net.au](mailto:logic123@bigpond.net.au]) 
  
**Bathroom Ware**  
Plumbers Supply Co  
[1300 772 667](tel:1300772667)  
[www.pscoop.com.au](www.pscoop.com.au)  
  
**Roofing**  
Dynamic Bradview Roofing  
[07 3284 5009](tel:0732845009)   
[sales@dbroofing.com.au](mailto:sales@dbroofing.com.au)  
  
**Tiles**  
Beaumont Tiles  
[07 5526 9444](tel:07 5526 9444)  
[bundall@tile.com.au](mailto:bundall@tile.com.au)  
  
**Benchtops**  
GMG Stone   
[gary@gmgstone.com.au](mailto:gary@gmgstone.com.au) 
Please contact Sunland Group in the event you cannot contact GMG Stone  
  
**Termite Protection**  
Rentokill  
[1300 855 822](tel:1300855822)  
[termite-au@rentokil.com](mailto:termite-au@rentokil.com)  
  
**Blinds (if applicable)**  
PR Corporate Blinds  
[07 5597 7055](tel:0755977055 ) 
[prcorporateblinds@gmail.com](mailto:prcorporateblinds@gmail.com)  